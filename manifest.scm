#!/usr/bin/env -S guix shell -m
!#

;;; Recipe to provide GNU Emacs through Guix

(use-modules
	(guix channels))

;; DNR-CI(Krey): Needs handling to keep the hash up-to-date
(list (channel
       (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (commit
	  "833febb522bd37a38156b558b108372b89a34949")
        (introduction
          (make-channel-introduction
            "9edb3f66fd807b096b48283debdcddccfea34bad"
            (openpgp-fingerprint
              "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA")))))

(specifications->manifest
	(list
	  "freecad" ; 930dd9a76203a3260b1e6256c70c1c3cad8c5cb8
	  ;; DEP(Krey): Used for working with openscad designs
	  "openscad"
	  ;; DEP(Krey): Required for working with FEM workbench
	  "gmsh"))
