
--- WORK IN PROGRESS RESOURCE! DO NOT USE IN PRODUCTION! ---

This is a list of materials that are acceptable for the chamber construction:

## Acknowledgements

The standard biggest wall is 1000x800 mm with pressure of 25 Pascals and projected use of heated magnetic stirrer set to max temperature 393.15 K (120 C) in an enviroment where it may be exposed to various chemicals, thus the requirements are set as follows:

Transparent solid material plate 1200x1000 mm with thickness ranging from 1.00 mm to 3.00 mm (max 5.00 mm) with weight less than 2 kg in a common earth environment subjected to pressure of 35 Pascals at continuous operating temperature 423.15 K (150 C) not surpassing plasticity (permanent deformation of the material) with displacement less than 0.50 μm for at least 10 minutes with chemical resistance to common chemical substances. Non-transparent material may be used with combination of transparent material for the Window plate to enable operation.

## Compatible Materials

### Poly Ether Ether Ketone (PEEK)

Material is compatible with requirements, but may require additives for transparency[1] or use with transparent material for window plate.

### Polyetherimide (PEI)

Amber-to-transparent solid, may not be a good option if it's transparency impairs operation which is unlikely to do.

It is prone to stress cracking when exposed to chlorinated solvets[2], if application includes these then it's chemical markup needs to be handled appropriately (additives to manage this issue).

### Polysulfone (PSU)

Material is compatible with requirements, but note that it is known to dissolve in dichloromethane and methylpyrrolidone[3].

## Incompatible materials

### Acrylate polymer (acrylic glass)

It's Continuous Operating Temperature is around 348.15 K (75 C) which is not suitable for the use with heater element inside the acrylic for a long period of time. Projected failure is plasticity if the chamber is pressurized or formation of bubbles due to the overheat of the material.

## References

1. Research paper on "Transparency enhancement in semicrystalline PEEK through variation of polymer morphology" -- https://onlinelibrary.wiley.com/doi/10.1002/(SICI)1099-0488(199603)34:4%3C707::AID-POLB11%3E3.0.CO;2-L
2. Wikipage on the properties of Polyethermide -- https://en.wikipedia.org/wiki/Polyetherimide#Properties
3. Wikipage on the properties of Polysulfone -- https://en.wikipedia.org/wiki/Polysulfone#Properties
